package Excercise08;
public class Data {

	public static void main(String[] args) {
		int N = 5;
		ArithmeticAverage average;

		average = new ArithmeticAverage(N);
		
		System.out.println("Generating data");
		for(int i=0;i<N/2;i++) {
			average.setNumber((int) (Math.random()*1000), (int) (Math.random()*N)); 
		}
		
		System.out.println("The arithmetic average of " + average.toString() + "\nis " + average.computeArithmeticAverage());
	}
}
