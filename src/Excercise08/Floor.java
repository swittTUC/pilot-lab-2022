package Excercise08;

import java.util.List;
import java.util.Vector;

/**
 * Diese Klasse beschreibt eine Etage in dem Gebäude. Sie Verfügt über eine Test Anzahl an Raumen.
 * Zudem soll immer einer der Räume über einen Zugang zum Fahrstuhl verfügen. Alles dies wird im Konstruktor festgelegt.
 * @author swittek
 *
 */
public class Floor {
	private List<Room> rooms;
	
	/**
	 * Konstruktor des Fahrstuhls. Erstellt alle Raume der Etage. 
	 * Der Raum mit dem Fahrstuhl wird mit elevatorRoomId angegeben.
	 * Der Raum mit dem Fahrstuhl hat keine Fenster.
	 * Alle anderen Räume haben immer 2 Fenster.
	 * 
	 * Im Konstruktore wird auch die Raumnummer berechnet. 
	 * Diese besteht immer aus einer hunderter Stelle, für die Etage + die Nummer des Raumes auf der Etage.
	 * z.B. der 3.  Raum auf der 4. Etage ist 403,
	 * 		der 10. Raum auf der 9. Etage ist 910
	 * 	 
	 * @param floorNumber Die Nummer der Etage.
	 * @param NumberOfRooms Die Anzahl der Raume, die angelegt werden soll.
	 * @param elevatorRoomId Der Index (innerhlab des Arrays) des Raumes, der den Fahrstuhlzugang erhalten soll.
	 * @param elevator Eine Referenz auf den Fahrstuhl des Gebäudes.
	 */
	public Floor(int floorNumber, int NumberOfRooms, int elevatorRoomId, Elevator elevator) {		
		this.rooms = new Vector<Room>();
		for (int i = 0; i < NumberOfRooms; i++) {
			if(i == elevatorRoomId) {
				rooms.add(new Room(floorNumber*100+1, 0, elevator));
			} else {
				rooms.add(new Room(floorNumber*100+1, 2, null));
			}
		}
	}
	
	/**
	 * @param roomIndex Position des Raumes im Array
	 * @return Raum mit dem Angegeben roomIndex
	 */
	public Room getRoom(int roomIndex) {
		return rooms.get(roomIndex);
	}
	
	/**
	 * @return Anzahl der Raume auf der Etage
	 */
	public int getRoomCount() {
		return rooms.size();
	}

}
