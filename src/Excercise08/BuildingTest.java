package Excercise08;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BuildingTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Der Test Erstellt ein Gebäude und prüft, ob es auf jeder Etage einen Raum mit einem Fahrstuhl gibt.
	 */
	@Test
	public void testBuilding() {
		Building university = new Building(100, 20);
		
				
		for(int floorNumber = 0; floorNumber<university.getFloorCount(); floorNumber++) {
			boolean floorHasElevator = false;
			for(int roomNumber = 0; roomNumber<university.getFloor(floorNumber).getRoomCount(); roomNumber++) {
				floorHasElevator = floorHasElevator || university.getFloor(floorNumber).getRoom(roomNumber).hasElevator();
			}
			assert(floorHasElevator);
		}
		
		
	}

}