package Excercise08;

import java.util.List;
import java.util.Vector;

/**
 * Diese Klasse Beschreibt einen 
 * @author swittek
 *
 */
public class Building {
	private List<Floor> floors;
	private Elevator elevator;
	
	/**
	 * Hier wird das Gebäude erstellt und seine Etagen angelegt. 
	 * Der Zugang zum Aufzug soll immer bei dem letzen Raum einer Etage erfolgen.
	 * @param numberOfFloors Anzahl der Etagen.
	 * @param roomsPerFloor Anzahl der Raume in jeder Etage
	 */
	public Building(int  numberOfFloors, int roomsPerFloor) {
		this.elevator = new Elevator();		
		this.floors = new Vector<Floor>();		
		
		for (int i = 0; i < numberOfFloors; i++) {			
			this.floors.add(new Floor(i, numberOfFloors, roomsPerFloor-1, elevator));			
		}
	}
	
	/**
	 * @param floorNumber Position der gesuchten Etage im Array.
	 * @return Etage mit der Angegebenen Nummer;
	 */
	public Floor getFloor(int floorNumber) {
		return floors.get(floorNumber);
	}
	
	/**	
	 * @return Anzahl der Etagen.
	 */
	public int getFloorCount() {
		return this.floors.size();
	}
}
