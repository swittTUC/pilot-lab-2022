package Excercise08;

/**
 * Class that can calculate the mean of a set of integers.
 *
 */
public class ArithmeticAverage {
	/**
	 *  Array with numbers from which the mean value can be calculated.
	 */
	private int[] numbers;

	/**
	 * Creates a new object, which can store at most the given number of Integeres.
	 * can store.
	 * 
	 * @param numberOfNumbers
	 * Number of numbers that can be stored at most.
	 */
	public ArithmeticAverage(int numberOfNumbers) {
		numbers = new int[numberOfNumbers];
		clean();
	}

	/**
	 * Places the passed number at the parent position of the array. Already
	 * existing numbers are overwritten.
	 * 
	 * @param number Number to be set. This must be greater than 0, otherwise, it will not be set.
	 * 
	 * @param index Index to which the passed number is to be set.
	 */
	public void setNumber(int number, int index) {
		if (number > 0 && index >= 0 && index < numbers.length) {
			numbers[index] = number;
		}
	}

	/**
	 * Calculates the mean value of all numbers stored so far and returns
	 * returns this.
	 * 
	 * @return Average value of all numbers stored so far.
	 */
	public double computeArithmeticAverage() {
		double sum = 0;
		int counter = 0;

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] != -1) {
				sum += numbers[i];
				counter++;
			}
		}

		return sum / counter;
	}

	/**
	 * Returns a string with all stored numbers.
	 * 
	 * @return String with all stored numbers.
	 */
	public String toString() {
		String result = "[";

		for (int number : numbers) {
			if (number != -1) {
				result += number + ", ";
			}
		}

		result = result.substring(0, result.length() - 2) + "]";

		return result;
	}

	/**
	 * Removes all numbers already stored.
	 */
	public void clean() {
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = -1;
		}
	}
}
