package Excercise05;

/**
 * A room on one floor of the building. The room has a certain number of windows.
 */
public class Room {
	private Window[] windows;
	private int number;
	private Elevator elevator;
	
	/**
	 * Initiates the room and its windows.
	 * @param roomNumber Room number. This always consists of a hundreds digit, for the floor + the number of the room on the floor.
	 * e.g. the 3rd room on the 4th floor is 403,
	 * the 10th room on the 9th floor is 910
	 * 
	 * @param windowCount Number of windows.
	 * @param elevator A reference to the elevator, or null if the room has no elevator access.
	 */
	public Room(int roomNumber, int windowCount, Elevator elevator) {
		// Implementieren Sie den Konstruktor.
	}

	/**
	 * @return Returns whether the room has elevator access.
	 */
	public boolean hasElevator() {
		return elevator !=null;
	}
}
