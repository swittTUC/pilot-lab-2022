package Excercise05;

/**
 * This class describes a floor in the building. It has a test number of rooms.
 * Also, one of the rooms should always have access to the elevator. All this is defined in the constructor.
 * 
 * this 
 * @author Sebastian
 *
 */
public class Floor {
	private Room[] rooms;
	
	/**
	 * Constructor of the elevator. Creates all rooms of the floor. 
	 * The room with the elevator is specified with elevatorRoomId.
	 * The room with the elevator has no windows.
	 * All other rooms always have 2 windows.
	 * 
	 * The room number is also calculated in the constructor. 
	 * This always consists of a hundreds digit, for the floor + the number of the room on the floor.
	 * e.g. the 3rd room on the 4th floor is 403,
	 * the 10th room on the 9th floor is 910
	 * 	 
	 * @param floorNumber The number of the floor.
	 * @param NumberOfRooms The number of rooms to create.
	 * @param elevatorRoomId The index (within the array) of the room that should get the elevator access.
	 * @param elevator A reference to the elevator of the building.
	 */
	public Floor(int floorNumber, int NumberOfRooms, int elevatorRoomId, Elevator elevator) {	
		// Implementieren Sie den Konstruktor.
	}
	
	/**
	 * @param roomIndex Position of the room in the array
	 * @return room with the given roomIndex
	 */
	public Room getRoom(int roomIndex) {
		return rooms[roomIndex];
	}
	
	/**
	 * @return number of rooms on the floor
	 */
	public int getRoomCount() {
		return rooms.length;
	}

}
