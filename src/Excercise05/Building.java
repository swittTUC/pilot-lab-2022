package Excercise05;

/**
 * The Class represent a Building *
 */
public class Building {
	private Floor[] floors;
	private Elevator elevator;
	
	/**
	 * Creats the Floors.
	 * The evlevator is supposed to be in the Last Room of each floor.
	 * @param numberOfFloors
	 * @param roomsPerFloor 
	 */
	public Building() {
	// implement
	}
	
	
	/**
	 * @param floorNumber Number of Floor in Question
	 * @return the Floor
	 */
	public Floor getFloor(int floorNumber) {
		return floors[floorNumber];
	}
	
	/**	
	 * @return Number of Floors.
	 */
	public int getFloorCount() {
		return this.floors.length;
	}
}
