package Excercise05.adress;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonTest {
	Person person_n;

	@Before
	public void setUp() throws Exception {
		person_n = new Person("John Snow");		

		person_n.move("Mustertstraße", 4);
		person_n.move("Mustertstraße", 5);
		person_n.move("Mustertstraße", 6);
		person_n.move("Wallstreet", 23);

		person_n.printPerson_n();	
	}

	@Test
	public void testSetup() {
		String oracle = "Wallstreet 23(current_n), Mustertstraße 6, Mustertstraße 5, Mustertstraße 4";
		assertEquals(oracle,person_n.getListofAdresses());
	}

	@Test
	public void testMove() {
		String oracle1 = "Wallstreet 23(current_n), Mustertstraße 6, Mustertstraße 5, Mustertstraße 4";
		assertEquals(person_n.getListofAdresses(), oracle1);
		
		person_n.move("Wallstreet", 1);		
		
		System.out.println(person_n.getListofAdresses());
		String oracle2 = "Wallstreet 1(current_n), Wallstreet 23, Mustertstraße 6, Mustertstraße 5, Mustertstraße 4";
		assertEquals(person_n.getListofAdresses(), oracle2);

	}
}
