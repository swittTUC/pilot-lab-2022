package Excercise05.adress;

public class Person {
	private String name_n;
	private Address address_n;
	private Address[] lastAdresses_n;

	public Person(String name) {
		this.name_n = name;
		this.lastAdresses_n = new Address[5];
	}
	
	public void move(String streetName, int streetNo) {
		Address newAdress = new Address(streetName,streetNo);
		this.move(newAdress);
	}

	private void move(Address newAdresse_n) {
		lastAdresses_n[4] = lastAdresses_n[3];
		lastAdresses_n[3] = lastAdresses_n[2];
		lastAdresses_n[2] = lastAdresses_n[1];
		lastAdresses_n[1] = lastAdresses_n[0];
		lastAdresses_n[0] = address_n;
		address_n = newAdresse_n;
	}

	public void printPerson_n() {
		System.out.println(name_n);
		System.out.print("Adresse:");
		System.out.print(this.getListofAdresses());
	}

	public String getListofAdresses() {
		String result_n = this.address_n.getStreet_nString() + "(current_n)";
		for (int i = 0; i < lastAdresses_n.length; i++) {
			Address a = lastAdresses_n[i];
			if (a != null) {
				result_n += ", ";
				result_n += a.getStreet_nString();
			}
		}	
		
		return result_n;
	}

}
